/**
 * 4. Чему будет равна переменная "a" после выполнения этого участка кода?
 * Объясните почему.
 */
let a = 1;
function foo() {

    a = 2;
    return 10;
}

// Здесь а = 1;
// Далее, внутри функции "a" присваевается 2, но пока не вызвали ф-ю - "а", объявленная в самом верху - не переприсваивается
// После сложения текущей, неизмененной "а" (1) с результатом ф-ии foo (10) - "а" становится 1 + 10 = 11
a += foo(); // 11

// Тут все тоже самое, но начальное значение "а" - 11
// внутри функции "a" присваевается 2, но пока не вызвали ф-ю - "а", переприсвоенная в предыдущей операции не затирается переприсвоением внутри foo
// После сложения текущей, неизмененной "а" (11) с результатом ф-ии foo (10) - "а" становится 11 + 10 = 21
a += foo(); // 21

// Данное поведение не зависит от метода объявления var или let