/**
 * 3. Какая проблема может быть с этим кодом, если список очень длинный?
 * Предложите и объясните свое решение
 */

/*
* Рекурсивные вызовы в js не оптимизированы и может возникнуть
* исключение Maximum call stack size exceeded (будет постоянно увеличиваться стек вызовов)
* Для того, чтобы этого не произошло необходимо использовать Proper Tail Call -
* это вызов функции из другой функции без увеличения стэка вызовов
* */

function trampFn(fn) {
    // Вызываем функцию с рекурсией, пока тип возвращаемого результата является функцией,
    while (typeof fn === "function") {
        fn = fn();
    }
    return fn;
};

const readHugeList = (length, min, max) => (
    [...new Array(length)]
        .map(() => Math.round(Math.random() * max) + min)
);


let list = readHugeList(1000000, 1, 100);

let i = 0;

let nextListItem = function () {
    let item = list.pop();
    if (item) {
        // ... обработка записи
        i += 1;

        // Возвращаем функцию
        return () => nextListItem()
    }
};

console.time('var1')

trampFn(nextListItem);
console.timeEnd('var1')
console.log(i)
