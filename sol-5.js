/**
 * 5. Сделайте рефакторинг кода для работы с API чужого сервиса
 */

const CURR_MAP = {
    RUB: 'руб',
    SOM: 'сом',
    TENGE: 'тенге'
}

function totalStrPrepare(total, inputCur) {
    let cur = CURR_MAP[inputCur] || CURR_MAP.RUB;
    return `${total} ${cur}.`
}

function printOrderTotal(responseString, curr) {

    let total = 0;
    let items = [];
    let result = '';

    const orderPriceStr = 'Стоимость заказа:';
    const freeStr = 'Бесплатно';
    const currStr = 'руб.';

    try {
        items = JSON.parse(responseString);
    } catch (error) {
        console.log(error)

        // Тут можно как-нибудь залогать ошибку парсинга (в sentry или другой логгер)

        // Ошибку желательно переформатировать для пользователя, поэтому выкидываем отформатированную
        throw new Error('unexpected parse error')
    }

    items.forEach(item=> {
        const price = parseFloat(item.price) || 0;
        console.log(price)
        total += price;
    });

    // Не очень понял, что имелось в виду тут:
    // ("Стоимость заказа: "+ total >0 ? "Бесплатно": total + " руб.")
    // если total > 0, то бесплатно отдаем товар?
    // Думаю наоборот все-таки: если общая стоимость товаров > 0, то мы за эту цену и продаем
    // Как раз ситуация для уточнения требований
    const totalStr = total > 0 ? totalStrPrepare(total, curr) : freeStr;
    result = `${orderPriceStr} ${totalStr}`

    console.log(result);

    // console.log("Стоимость заказа: "+ total >0 ? "Бесплатно": total + " руб.");
    // Эта строка веселая была - конкатенация строки и числа = неприводимая к числу строка
    // Сравнение неприводимой к числу строки и числа всегда дает false
}

// Все ок
printOrderTotal('[{ "price": 19}, {"price": 15.07}, { "a": "o"}, { "price": "123"}, {"price": "Сколько-то рублей"}]', 'SOM')

// Ошибка парсинга json
// printOrderTotal('[{ "price": 13}, {"price": 15}, { "a": "o"}, { price: "123"}, {"price": "Сколько-то рублей"}]')